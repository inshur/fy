# Fye

## Description

A tool which deploys infrastructure and application manifests for: AWS, GCP, and Kubernetes.

## Install

```bash
# FIXME: enable os-specific install by replacing with: curl | bash
brew install python3
pip3 install pipx
pipx install https://bitbucket.org/inshur/fy
```

## Configure

```bash
export VAULT_ADDR="https://vault.infra.inshur.com"
export VAULT_ROLE="robw"
export VAULT_TOKEN="..."
```

## Usage

```text
usage:
    fy <command> [-h|--help|-v|--version]

commands:
    env        establish environment from deployment context
    skeleton   manage deployment context symlinks and boilerplate manifests
    vault      manage access to deployment target via vault
    deploy     manage infrastructure from deployment context

positional arguments:
  command        subcommand to run

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```

## Using Primitives

Although `fy deploy <command>` can be used for the most common workflows some operations require using the tools that `fy` wraps. In these cases it's possible to use `fy` to configure the shell environment so that these tools will work as expected, for example:
```bash
source <(fy env sh --raw)
fy vault login
fy vault read
terraform apply
kubectl apply -f manifest.yml
```
