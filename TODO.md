# Plan of Work

## Misc.
* wrap all `sh` imports and advise about tool install..
* package `fy(1)`
* create simple `curl|bash` style installation mechanism for `fy(1)`
* implement kuber subcommand to enable deploying kubernetes applications
* error handling
* implement `fy repo` to configure new git repositories with docker access?
* default k8s cluster deployment with NAT gateway
* handle helm charts

## For `fy vault`
* improve error handling around `fy vault [list|revoke]`
* implement `fy vault roll-key`
* implement vault login - maybe part of general `fy configure` subcommand?
* implement AWS support
* clean-up vault terraform manifests
* create vault users rather than using root role

## For `fy deploy`
* decide whether implementing `--skip-vault`, `--skip-skeleton`, `--skip-terraform`, and `--skip-k8s` as commands to `deploy` subcommand makes sense
* implement `--k8s-flags="..."` and `--terraform-flags="..."` - is this necessary?
* rename `plan` to `dry-run` so as to not confuse pass-through commands as directly mapped?
