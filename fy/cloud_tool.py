#!/usr/bin/env python

import re
import json
from dataclasses import dataclass
from .environment import Environment

try:
    from sh import gcloud
except ImportError as error:
    if re.search(r".*'gcloud'.*", str(error)):
        print("could not find gcloud(1) in path, please install GCP SDK!")
        exit(127)
    raise ImportError(error)


@dataclass
class CloudTool:
    def reset_account_state(self):
        raise Exception("not implemented!")


@dataclass
class GCPCloudTool(CloudTool):
    environment: str = Environment

    def configure(self, service_account_email):
        print(f"activating service account: {service_account_email}")
        stdout = gcloud.auth(
            "activate-service-account", "--key-file", self.environment.credentials_file
        ).stdout.decode("ASCII")
        if stdout:
            print(stdout)

    def client_email(self):
        with open(self.environment.credentials_file, "r") as file:
            return json.load(file)["client_email"]

    def reset_account_state(self):
        if self.environment.gcp_account_original:
            print(
                f"reactivating original service account: {self.environment.gcp_account_original}"
            )
            print(
                gcloud.config.set.account(
                    self.environment.gcp_account_original
                ).stderr.decode("ASCII")
            )
        else:
            print("deactivating service account")
            print(gcloud.config.unset.account().stderr.decode("ASCII"))


@dataclass
class AWSCloudTool(CloudTool):
    environment: Environment

    def configure(self):
        pass
