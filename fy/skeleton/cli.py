#!/usr/bin/env python

import os
import sys
from pathlib import Path
from textwrap import dedent
from dataclasses import dataclass

from ..extended_help_argument_parser import ExtendedHelpArgumentParser
from ..environment.factory import environment
from .skeleton import Skeleton


@dataclass
class SkeletonCLI:
    def __init__(self, command):
        parser = ExtendedHelpArgumentParser(
            usage=dedent(
                """
                  fy deploy <command> [-h|--help]

                commands:
                  apply      apply skeleton to current directory
                  clean      remove skeleton from current directory
                  refresh    run clean then apply
                """
            ),
        )

        parser.add_argument("command", help="subcommand to run")
        args = parser.parse_args(sys.argv[2:3])
        sub_command = args.command

        try:
            getattr(self, sub_command)()
        except AttributeError:
            parser.error(f"command not found: {args.sub_command}")

    def apply(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy skeleton plan [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        print("\n==> skeleton apply\n")
        Skeleton().apply()

    def clean(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy skeleton clean [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        print("\n==> skeleton clean\n")
        Skeleton().clean()

    def refresh(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy skeleton refresh [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        self.clean()
        self.apply()

    def _set_path(self):
        self.path = Path(
            os.path.join(
                environment().deployment_path,
                "../../../../bootstrap/deployment/skeleton/",
            )
        )
