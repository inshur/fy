#!/usr/bin/env python

import sys
import traceback
from typing import List
from textwrap import dedent
from dataclasses import dataclass, field

from ..extended_help_argument_parser import ExtendedHelpArgumentParser
from ..environment.factory import environment
from ..skeleton.skeleton import Skeleton
from ..vault.vault_leases import VaultLeases
from ..terraform.terraform import Terraform


@dataclass
class DeployCLI:
    vault_leases: List[VaultLeases] = field(default_factory=List)

    def __init__(self, command):
        parser = ExtendedHelpArgumentParser(
            usage=dedent(
                """
                  fy deploy <command> [-h|--help]

                commands:
                  init       initialize deployment with remote backend state
                  plan       dry-run
                  apply      apply deployment
                  destroy    destroy deployment
                """
            )
        )

        parser.add_argument("command", help="subcommand to run")
        args = parser.parse_args(sys.argv[2:3])
        sub_command = args.command

        try:
            getattr(self, sub_command)()
        except AttributeError:
            parser.error(f"command not found: {args.sub_command}")

    def setup(self):
        print("\n==> deploy environment\n")
        print(environment().pretty_print(obfuscate=True))
        # FIXME: allow skipping
        print("\n==> skeleton refresh\n")
        Skeleton().clean()
        print()
        Skeleton().apply()
        # FIXME: allow skipping
        print("\n==> vault refresh\n")
        self.vault_leases = VaultLeases()
        self.vault_leases.read()

    def handle_exception(self, error):
        print("\n==> exception caught!")
        print("\n==> initializing clean-up")
        self.vault_leases.revoke()
        print("\n==> stack trace\n")
        traceback.print_tb(error.__traceback__)

    def init(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy deploy plan [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        try:
            self.setup()
            print("\n==> terraform init")
            Terraform().init()
            print("\n==> initializing clean-up")
            self.vault_leases.revoke()
        except Exception as error:
            self.handle_exception(error)

    def plan(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy deploy plan [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        try:
            self.setup()
            print("\n==> terraform init")
            Terraform().init()
            print("\n==> terraform plan\n")
            Terraform().plan()
            print("\n==> initializing clean-up")
            self.vault_leases.revoke()
        except Exception as error:
            self.handle_exception(error)

    def plan_apply(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy deploy plan [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        try:
            self.setup()
            print("\n==> terraform init")
            Terraform().init()
            print("\n==> terraform plan\n")
            Terraform().plan()
            print("\n==> terraform apply\n")
            Terraform().apply()
            print("\n==> initializing clean-up")
            self.vault_leases.revoke()
        except Exception as error:
            self.handle_exception(error)

    def apply(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy deploy apply [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        try:
            self.setup()
            print("\n==> terraform init")
            Terraform().init()
            print("\n==> terraform apply\n")
            Terraform().apply()
            print("\n==> initializing clean-up")
            self.vault_leases.revoke()
        except Exception as error:
            self.handle_exception(error)

    def destroy(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy deploy destroy [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        self.setup()
        Terraform().destroy()
