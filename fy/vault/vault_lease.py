#!/usr/bin/env python

import re
import sys
import json
from dataclasses import dataclass, field


try:
    from sh import ErrorReturnCode_2, vault
except ImportError as error:
    if re.search(r".*'vault'.*", str(error)):
        print("could not find vault(1) in path, please install vault!")
        exit(127)
    raise ImportError(error)


class GCPServiceAccountKeyRateLimitError(Exception):
    pass


@dataclass
class VaultLease:
    path: str = field(init=False)
    lease: str = field(init=False)
    lease_id: str = field(init=False)

    def __str__(self):
        return f"vault path: {self.path}, lease id: {self.lease_id}]"

    def revoke(self):
        print(f"\nvault lease revoke {self.lease_id}")
        vault.lease.revoke(self.lease_id, _fg=True)
        self._revoke_cleanup()

    def _set_lease(self):
        try:
            print(f"vault read {self.path}")
            self.lease = json.loads(
                vault.read("-format=json", self.path, _err=sys.stderr).stdout.decode(
                    "ASCII"
                )
            )
        except ErrorReturnCode_2 as error:
            if re.search(r".*Maximum number of keys on account reached.*", str(error)):
                raise GCPServiceAccountKeyRateLimitError(
                    "Maximum number of service account keys reached"
                )
            else:
                raise error

    def _set_lease_id(self, silent=False):
        self.lease_id = self.lease["lease_id"]
        if not silent:
            print(f"vault lease id: {self.lease_id}")
