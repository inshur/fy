#!/usr/bin/env python

import sys
from textwrap import dedent
from dataclasses import dataclass

from ..extended_help_argument_parser import ExtendedHelpArgumentParser
from .vault_leases import VaultLeases


@dataclass
class VaultCLI:
    def __init__(self, command):
        parser = ExtendedHelpArgumentParser(
            usage=dedent(
                """
                  fy vault <command> [-h|--help]

                commands:
                  login      login to vault to retrieve vault token
                  read       read new cloud credentials
                  revoke     revoke local cloud credentials
                  list       list local credentials
                  roll-key   roll service-account access key
                """
            ),
        )

        parser.add_argument("command", help="subcommand to run")
        args = parser.parse_args(sys.argv[2:3])
        sub_command = args.command

        try:
            getattr(self, sub_command)()
        except AttributeError:
            parser.error(f"command not found: {args.sub_command}")

    def login(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy vault login [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        print("login..")
        exit(0)

    def read(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy vault read [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        print("\n==> vault read\n")
        VaultLeases().read()
        exit(0)

    def revoke(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy vault revoke [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        vault_leases = VaultLeases()
        vault_leases.load()
        if vault_leases.leases:
            print("\n==> vault revoke")
            vault_leases.revoke()
        exit(0)

    def list(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy vault list [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        vault_leases = VaultLeases()
        vault_leases.load()
        if vault_leases.leases:
            print("\n==> vault list\n")
        vault_leases.list()
        exit(0)

    def roll_key(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy vault list [-h|--help]")
        parser.parse_args(sys.argv[3:4])
        print("roll-key")
        exit(0)
