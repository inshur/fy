#!/usr/bin/env python

from typing import List
from dataclasses import dataclass, field
from ..environment.factory import environment
from .lease_factory import vault_lease
from .gcp_lease import GCPVaultLease
from .aws_lease import AWSVaultLease
from .vault_lease import VaultLease


@dataclass
class VaultLeases:
    leases: List[VaultLease] = field(default_factory=list)

    def login(self):
        pass

    def load(self):
        self._load_default_credentials()
        self._load_additional_credentials()

    def _load_default_credentials(self):
        lease = vault_lease()
        self.leases.append(lease)
        lease.load()

    def _load_additional_credentials(self):
        for project in environment().additional_projects:
            if environment().platform == "GCP":
                lease = GCPVaultLease(
                    project_id=project["project_id"], role=project["vault_role"]
                )
                self.leases.append(lease)
                lease.load()
            elif environment().platform == "AWS":
                pass
                # vault_lease(project_id: environment().project_id, role: environment().vault_role).read()
            else:
                pass

    def read(self):
        self._read_default_credentials()
        self._read_additional_credentials()

    def _read_default_credentials(self):
        lease = vault_lease()
        self.leases.append(lease)
        lease.read()

    def _read_additional_credentials(self):
        for project in environment().additional_projects:
            print()
            if environment().platform == "GCP":
                lease = GCPVaultLease(
                    project_id=project["project_id"], role=project["vault_role"]
                )
                self.leases.append(lease)
                lease.read()
            elif environment().platform == "AWS":
                pass
                # vault_lease(project_id: environment().project_id, role: environment().vault_role).read()
            else:
                pass

    def revoke(self):
        for lease in self.leases:
            print
            lease.revoke()

    def list(self):
        for lease in self.leases:
            print(lease)
