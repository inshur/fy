#!/usr/bin/env python

import os
from sh import Command
from dataclasses import dataclass, field

from ..environment.factory import environment


@dataclass
class Terraform:
    terraform: Command = field(default_factory=Command)

    def __init__(self):
        env = {
            **os.environ.copy(),
            **{
                "GOOGLE_APPLICATION_CREDENTIALS": environment().google_application_credentials,
                "TF_IN_AUTOMATION": "true",
            },
        }
        self.terraform = Command("terraform").bake(_env=env, _fg=True)

    def init(self):
        self.terraform.init(
            "-backend-config",
            f"bucket=terraform-state-inshur-{environment().environment}-{environment().deployment}",
            "-backend-config",
            "prefix=terraform.state",
        )

    def plan(self):
        self.terraform.plan()

    def apply(self):
        self.terraform.apply()
