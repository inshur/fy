#!/usr/bin/env python

import sys
from argparse import ArgumentParser


class UnrecognisedCommandError(Exception):
    pass


class ExtendedHelpArgumentParser(ArgumentParser):
    def error(self, message):
        sys.stderr.write("error: %s\n\n" % message)
        self.print_help()
        exit(2)
