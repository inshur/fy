#!/usr/bin/env python

import sys
from textwrap import dedent

from .environment.environment import EnvironmentError
from .environment.factory import PlatformDetectionError
from .extended_help_argument_parser import (
    UnrecognisedCommandError,
    ExtendedHelpArgumentParser,
)
from .version import __version__
from .environment.cli import EnvCLI  # noqa: F401
from .skeleton.cli import SkeletonCLI  # noqa: F401
from .deploy.cli import DeployCLI  # noqa: F401
from .vault.cli import VaultCLI  # noqa: F401


class DeepArgParser:
    def __init__(self):
        try:
            self.parse_args()
        except (
            PlatformDetectionError,
            EnvironmentError,
            UnrecognisedCommandError,
        ) as error:
            print(f"{error.__class__.__name__}: {error}")
            exit(1)

    def parse_args(self):
        parser = ExtendedHelpArgumentParser(
            usage=dedent(
                """
                  fy <command> [-h|--help|-v|--version]

                commands:
                  env        establish environment from deployment context
                  skeleton   manage deployment context symlinks and boilerplate manifests
                  vault      manage access to deployment target via vault
                  deploy     manage infrastructure from deployment context
                """
            ),
        )

        parser.add_argument("-v", "--version", action="version", version=__version__)
        parser.add_argument("command", help="subcommand to run")
        args = parser.parse_args(sys.argv[1:2])
        sub_command = args.command

        class_name = f"{sub_command.capitalize()}CLI"
        getattr(sys.modules[__name__], class_name)(sub_command)
