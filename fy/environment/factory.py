#!/usr/bin/env python

import os
import re

from .aws import AWSEnvironment
from .gcp import GCPEnvironment


class PlatformDetectionError(Exception):
    pass


def environment():
    if re.match(r".*/aws/deployments/.*", os.getcwd()):
        return AWSEnvironment(platform="AWS")
    if re.match(r".*/gcp/deployments/.*", os.getcwd()):
        return GCPEnvironment(platform="GCP")
    raise PlatformDetectionError(
        f"failed to detect platform from current working directory: {os.getcwd()}"
    )
