#!/usr/bin/env python

import os
import re
import json
from dataclasses import dataclass, field, asdict


class EnvironmentError(Exception):
    pass


@dataclass
class Environment:
    platform: str

    region: str = field(init=False)
    environment: str = field(init=False)
    deployment: str = field(init=False)

    vault_addr: str = field(init=False)
    vault_token: str = field(init=False)
    vault_role: str = field(init=False)

    deployment_path: str = os.getcwd()
    config_dir: str = os.path.join(os.environ["HOME"], ".config/fy")
    vault_dir: str = os.path.join(os.environ["HOME"], ".config/fy/vault")

    def properties(self, obfuscate):
        properties = self._filter_keys(asdict(self), ["vault_dir", "credentials_dir"])
        if obfuscate:
            self._obfuscate_values(properties, ["vault_token"])
        return properties

    def pretty_print(self, obfuscate: False):
        # FIXME: improve output with sections?
        return "\n".join(
            f"{key.ljust(31)}: {value}"
            for key, value in self.properties(obfuscate).items()
        )

    def sh(self, obfuscate: False):
        return "\n".join(
            f'{key.upper()}="{value}"'
            for key, value in self.properties(obfuscate).items()
        )

    def json(self, obfuscate: False):
        return json.dumps(self.properties(obfuscate))

    def json_upper_keys(self, obfuscate: False):
        return {key.upper(): value for key, value in self.properties(obfuscate).items()}

    def _filter_keys(self, properties, keys):
        return {key: value for key, value in asdict(self).items() if key not in keys}

    def _obfuscate_values(self, properties, keys):
        for key in keys:
            if properties[key]:
                properties[key] = "*" * len(properties[key])

    def _common_init(self):
        self._exit_unless_deployment()
        self._configure_deployment_environment()
        self._configure_vault_environment()

    def _configure_deployment_environment(self):
        deployment_path_elements = self.deployment_path.split("/")

        self.region = deployment_path_elements[-3]
        self.environment = deployment_path_elements[-2]
        self.deployment = deployment_path_elements[-1]

    def _configure_vault_environment(self):
        try:
            self.vault_role = os.environ["VAULT_ROLE"]
            self.vault_addr = os.environ["VAULT_ADDR"]
            self.vault_token = os.environ["VAULT_TOKEN"]
        except KeyError as error:
            raise EnvironmentError(f"variable not set: {error}")

    # FIXME: this could probably be imporoved by e.g: switching to pathlib
    def _exit_unless_deployment(self):
        if len(re.sub(".*\\/deployments\\/", "", self.deployment_path).split("/")) != 3:
            raise EnvironmentError(
                f"not in a valid deployment sub directory: {self.deployment_path}"
            )
