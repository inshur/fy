#!/usr/bin/env python

import os
import re
import json
from sh import aws

from dataclasses import dataclass, field, asdict
from .environment import Environment


@dataclass
class AWSEnvironment(Environment):
    def __post_init__(self):
        pass
