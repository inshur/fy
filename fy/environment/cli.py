#!/usr/bin/env python

import sys
from textwrap import dedent

from ..extended_help_argument_parser import ExtendedHelpArgumentParser
from .factory import environment


class EnvCLI:
    def __init__(self, command):
        parser = ExtendedHelpArgumentParser(
            usage=dedent(
                """
                  fy env <command> [-h|--help]

                commands:
                  pp         pretty print output
                  sh         output in shell sourceable format
                  json       output json format
                """
            ),
        )

        parser.add_argument("command", help="subcommand to run")
        args = parser.parse_args(sys.argv[2:3])
        sub_command = args.command

        # raise any errors early..
        environment()

        try:
            getattr(self, sub_command)()
        except AttributeError:
            parser.error(f"command not found: {args.sub_command}")

    def pp(self):
        parser = ExtendedHelpArgumentParser(
            usage="\n  fy env print [-h|--help|-r|--raw]"
        )
        parser.add_argument(
            "-r", "--raw", help="do not obfuscate secrets", action="store_false"
        )
        args = parser.parse_args(sys.argv[3:4])
        print("\n==> environment\n")
        print(environment().pretty_print(obfuscate=args.raw))
        exit(0)

    def sh(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy env sh [-h|--help|-r|--raw]")
        parser.add_argument(
            "-r", "--raw", help="do not obfuscate secrets", action="store_false"
        )
        args = parser.parse_args(sys.argv[3:4])
        print(environment().sh(obfuscate=args.raw))
        exit(0)

    def json(self):
        parser = ExtendedHelpArgumentParser(usage="\n  fy env json [-h|--help]")
        parser.add_argument(
            "-r", "--raw", help="do not obfuscate secrets", action="store_false"
        )
        args = parser.parse_args(sys.argv[3:4])
        print(environment().json(obfuscate=args.raw))
        exit(0)
