#!/usr/bin/env python

import os
import json
import yaml
from sh import gcloud
from typing import List
from pathlib import Path
from dataclasses import dataclass, field

from .environment import Environment


@dataclass
class GCPEnvironment(Environment):
    credentials_dir: str = field(init=False)
    gcp_account_original: str = field(init=False)
    project_id: str = field(init=False)
    additional_projects: List[str] = field(default_factory=list)
    google_application_credentials: str = field(init=False)

    def __post_init__(self):
        self._common_init()
        self._set_gcp_account_original()
        self._set_project_id()
        self._set_credentials_dir()
        self._set_additional_projects()
        self._set_google_application_credentials()

    def _set_google_application_credentials(self):
        self.google_application_credentials = os.path.join(
            self.credentials_dir, f"{self.project_id}_{self.vault_role}.json"
        )

    def _set_project_id(self):
        self.project_id = f"inshur-{self.environment}-{self.deployment}"

    def _set_credentials_dir(self):
        self.credentials_dir = os.path.join(
            os.environ["HOME"], self.config_dir, "gcp/credentials"
        )

    def _set_gcp_account_original(self):
        accounts = json.loads(gcloud.auth.list("--format", "json").stdout)
        active = [
            account["account"] for account in accounts if account["status"] == "ACTIVE"
        ]
        if active:
            self.gcp_account_original = active[0]
        else:
            self.gcp_account_original = ""

    def _set_additional_projects(self):
        collection = []
        for providers_file in Path(".").glob("*.providers.yml"):
            providers = yaml.safe_load(open(providers_file))["providers"]["gcp"]
            collection = collection + providers
        self.additional_projects = collection
